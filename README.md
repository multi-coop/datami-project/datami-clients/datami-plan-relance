# Visualiseur des données

Widget de prévisualisation des données issues de fichiers csv

---

## Résumé

...

Afin de pouvoir partager et de mettre en valeur toutes les ressources de ce repo il a été proposé de créer un outil numérique de type "widget" : `datami`. En effet un outil de ce type permet de pouvoir intégrer sur des sites tiers (sites de partenaires ou autres) une sélection plus ou moins large de ressources. Cette solution permet à la fois d'éviter aux sites partenaires de "copier-coller" les ressources, d'afficher sur ces sites tiers les ressources toujours à jour, et de permettre aux sites tiers ainsi qu'au site source de gagner en visibilité, en légitimité et en qualité d'information.

L'autre avantage de cette solution est qu'elle n'est déployée qu'une fois, mais que le widget peut être intégré et paramétré/personnalisé sur autant de sites tiers que l'on souhaite... gratuitement.

La solution proposée et réalisée ici s'appuie sur un projet open source porté par la coopérative numérique [**multi**](https://multi.coop) : le projet [Datami](https://datami.multi.coop).

---

## Démo

- Page html de démo : [![Netlify Status](https://api.netlify.com/api/v1/badges/d618386d-f14e-4bb5-8c6d-5396af727175/deploy-status)](https://app.netlify.com/sites/datami-demo-france-relance/deploys)
- url de démo :
  - DEMO : https://datami-demo-france-relance.netlify.app/

---

### Documentation

Le site officiel du projet Datami : https://datami.multi.coop

Un site dédié à la documentation technique de Datami est consultable ici : https://datami-docs.multi.coop

---

## Mini server pour développement local

Un mini serveur en Python est inclus pour développer et servir les fichiers localement : `server.py`

Pour installer le mini-serveur :

```sh
pip install --upgrade pip
python3 -m pip install --user virtualenv
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

ou

```sh
sh setup.sh
source venv/bin/activate
```

---

### Lancer le serveur local

Pour lancer le serveur sur `http://localhost:8851`:

```sh
python server.py
```

or

```sh
sh run_server.sh
```

Les fichiers sont servis localement sur :

- `http://localhost:8851/content/<path:folder_path>/<string:filename>`
- `http://localhost:8851/statics/<path:folder_path>/<string:filename>`

---

## Pour aller plus loin

### Datami

Le widget fait partie intégrante du projet [Datami](https://gitlab.com/multi-coop/datami)
